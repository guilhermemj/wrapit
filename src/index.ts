import axios from 'axios';
import { createMethod } from './create-method';

interface ApiOptions {
  baseUrl?: string,
  authMethod?: string,
  authToken?: string,
}

interface ApiMethodConfig {
  url: string,
  method?: string,
}

interface WrapitOptions extends ApiOptions {
  modules?: {
    [index: string]: {
      [index: string]: ApiMethodConfig,
    },
  },
}

export const wrapit = ({ modules = {}, ...options }: WrapitOptions) => class Api {
  [index: string]: any
  request: any

  constructor(apiOptions: ApiOptions = {}) {
    const config = { ...options, ...apiOptions };

    const {
      baseUrl = '',
      // authMethod = 'bearer-token',
      authToken,
    } = config;

    this.request = axios.create({
      baseURL: baseUrl,
    });

    if (authToken) {
      this.grantAuthorization(authToken);
    }

    Object.entries(modules).forEach(([key, value]) => {
      this.registerModule(key, value);
    });
  }

  // TODO: Make auth methods dynamic
  grantAuthorization(authToken: string) {
    this.request.defaults.headers.common.Authorization = `Bearer ${authToken}`;
  }

  revokeAuthorization() {
    delete this.request.defaults.headers.common.Authorization;
  }

  registerModule(namespace: string, methods: object) {
    const reservedNamespaces: string[] = [
      'request',
      'grantAuthorization',
      'revokeAuthorization',
      'registerModule',
    ];

    if (reservedNamespaces.includes(namespace)) {
      throw new RangeError(`Module namespace "${namespace}" is reserved.`);
    }

    const getMethod = (methodDef: any) => {
      if (typeof methodDef === 'function') {
        return methodDef(this.request);
      }

      return createMethod.call(this, methodDef);
    };

    this[namespace] = {};

    Object.entries(methods).forEach(([methodName, methodDef]) => {
      this[namespace][methodName] = getMethod(methodDef);
    });
  }
};
