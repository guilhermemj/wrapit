const PARAM_REGEX = /\:\w+/g;

const getParams = (url: string) => {
  const match = url.match(PARAM_REGEX) || [];

  return match.map((str: string) => str.replace(':', ''));
};

const parseUrl = (url: string) => {
  const newUrl = url.replace(PARAM_REGEX, (match) => {
    return `\${${match.replace(':', '')}}`;
  });

  return `\`${newUrl}\``;
};

interface ApiMethodConfig {
  url: string,
  method?: string,
}

export function createMethod ({ url, method = 'get' }: ApiMethodConfig) {
  const urlParams = getParams(url);

  const dataKey = ['post', 'patch', 'put'].includes(method) ? 'data' : 'params';

  const fnBody = `
    return this.request({
      ${dataKey}: ${dataKey},
      url: ${parseUrl(url)},
      method: '${method}'
    });
  `;

  const newFn = new Function(...urlParams.concat([dataKey, fnBody]));

  return newFn.bind(this);
};
