# wrAPIt

_A simple and convenient REST API wrapper._

## Instalation guide

1. Install package

``` bash
npm install @guilhermemj/wrapit
```

2. Import it in your file

``` javascript
// CommonJS
const { wrapit } = require('@guilhermemj/wrapit');

// ES6 Modules
import { wrapit } from '@guilhermemj/wrapit';
```

3. Wrap your api modules

``` javascript
const myApi = wrapit({
  modules: {
    // Your custom modules...
  },
});
```

**Work in progress...**
